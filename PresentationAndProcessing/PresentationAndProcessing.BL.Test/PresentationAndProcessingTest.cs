﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using PresentationAndProcessing.Data;
using PresentationAndProcessing.BL;

namespace PresentationAndProcessing.BL.Test
{
    [TestFixture]
    class PresentationAndProcessingTest
    {
        private DataProcessor _DP;

        [SetUp]
        public void Init()
        {
            _DP = new DataProcessor(@"C:\Users\про\Documents\Visual Studio 2013\Projects\PresentationAndProcessing\Example.txt");
        }

        [Test]
        public void GetPatient_PatientNameRequired()
        {

            string Name = _DP.GetPatient().Name;

            Assert.That(Name, Is.Not.Empty); //Assert
        }

        [Test]
        public void GetPatient_PatientSurnameRequired()
        {


            string Surname = _DP.GetPatient().Surname;

            Assert.That(Surname, Is.Not.Empty); //Assert
        }

        [Test]
        public void GetPatient_PatientPatronymicRequired()
        {


            string Patronymic = _DP.GetPatient().Patronymic;

            Assert.That(Patronymic, Is.Not.Empty); //Assert
        }

        [Test]
        public void GetPatient_DataRequired()
        {

            Dictionary<DateTime, PacientInformation> info = _DP.GetPatient().Data;



            Assert.That(info, Is.Not.Empty);
        }

        [Test]
        public void GetPatient_PatientTimeRequired()
        {

            PacientInformation s = _DP.GetDataInfo((Convert.ToDateTime("11.08.2016 8:03:05")));

            Assert.That(s, Is.Not.Null); //Assert
        }

        [Test]
        public void GetPatient_PatientCullingRequired()
        {

            _DP.Cilling(_DP.GetPatient().Data);

            foreach (var param in _DP.GetPatient().Data.Values)
            {
                Assert.That(param.HR, Is.InRange(40, 150)); //Assert
                Assert.That(param.LBP, Is.InRange(40, 140));
                Assert.That(param.UBP, Is.InRange(60, 260));
            }
        }

        [Test]
        public void GetInfomationTest()
        {         
            Dictionary<DateTime, PacientInformation> result = _DP.GetInfo(DateTime.Now, DateTime.Now);

            Assert.That(result, Is.Not.Empty);
        }

        [Test]
        public void GetPatient_PatientPHPRequired()
        {

            _DP.SetPHP(_DP.GetPatient().Data); //Act

            foreach (var inf in _DP.GetPatient().Data.Values)
            {
                Assert.That(inf.PHP, Is.Not.EqualTo(0)); //Assert
            }

        }

        [Test]
        public void GetPatient_PatientMHPRequired()
        {

            _DP.SetMHP(_DP.GetPatient().Data); //Act


            foreach (var inf in _DP.GetPatient().Data.Values)
            {
                Assert.That(inf.MHP, Is.Not.EqualTo(0)); //Assert
            }
        }
    }
}
