﻿namespace PresentationAndProcessing
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblPatronymic = new System.Windows.Forms.Label();
            this.comboBDate = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboEDate = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.AverBP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PBPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LBPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UBPColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.HRColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dGV = new System.Windows.Forms.DataGridView();
            this.cmbWeek = new System.Windows.Forms.ComboBox();
            this.btnArr = new System.Windows.Forms.Button();
            this.cBEvening = new System.Windows.Forms.CheckBox();
            this.cBDay = new System.Windows.Forms.CheckBox();
            this.cBMoning = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dGV)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button1.Location = new System.Drawing.Point(15, 424);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 33);
            this.button1.TabIndex = 0;
            this.button1.Text = "Загрузить данные";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Фамилия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Отчество";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(75, 63);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(10, 13);
            this.lblSurname.TabIndex = 4;
            this.lblSurname.Text = "-";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(75, 39);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(10, 13);
            this.lblName.TabIndex = 5;
            this.lblName.Text = "-";
            // 
            // lblPatronymic
            // 
            this.lblPatronymic.AutoSize = true;
            this.lblPatronymic.Location = new System.Drawing.Point(75, 90);
            this.lblPatronymic.Name = "lblPatronymic";
            this.lblPatronymic.Size = new System.Drawing.Size(10, 13);
            this.lblPatronymic.TabIndex = 6;
            this.lblPatronymic.Text = "-";
            // 
            // comboBDate
            // 
            this.comboBDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBDate.Enabled = false;
            this.comboBDate.FormattingEnabled = true;
            this.comboBDate.Location = new System.Drawing.Point(399, 25);
            this.comboBDate.Name = "comboBDate";
            this.comboBDate.Size = new System.Drawing.Size(121, 21);
            this.comboBDate.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(463, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Показать статистику за период";
            // 
            // comboEDate
            // 
            this.comboEDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboEDate.Enabled = false;
            this.comboEDate.FormattingEnabled = true;
            this.comboEDate.Location = new System.Drawing.Point(554, 25);
            this.comboEDate.Name = "comboEDate";
            this.comboEDate.Size = new System.Drawing.Size(121, 21);
            this.comboEDate.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(363, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "От";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(526, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(22, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "До";
            // 
            // btnFilter
            // 
            this.btnFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFilter.Enabled = false;
            this.btnFilter.Location = new System.Drawing.Point(497, 108);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(80, 35);
            this.btnFilter.TabIndex = 11;
            this.btnFilter.Text = "ОК";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // AverBP
            // 
            this.AverBP.HeaderText = "СрАД";
            this.AverBP.Name = "AverBP";
            // 
            // PBPColumn
            // 
            this.PBPColumn.HeaderText = "ПАД";
            this.PBPColumn.Name = "PBPColumn";
            // 
            // LBPColumn
            // 
            this.LBPColumn.HeaderText = "ДАД";
            this.LBPColumn.Name = "LBPColumn";
            // 
            // UBPColumn
            // 
            this.UBPColumn.HeaderText = "САД";
            this.UBPColumn.Name = "UBPColumn";
            // 
            // HRColumn
            // 
            this.HRColumn.HeaderText = "ЧСС";
            this.HRColumn.Name = "HRColumn";
            // 
            // DateColumn
            // 
            this.DateColumn.HeaderText = "Дата";
            this.DateColumn.Name = "DateColumn";
            // 
            // dGV
            // 
            this.dGV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DateColumn,
            this.HRColumn,
            this.UBPColumn,
            this.LBPColumn,
            this.PBPColumn,
            this.AverBP});
            this.dGV.Location = new System.Drawing.Point(15, 155);
            this.dGV.Name = "dGV";
            this.dGV.Size = new System.Drawing.Size(643, 264);
            this.dGV.TabIndex = 12;
            // 
            // cmbWeek
            // 
            this.cmbWeek.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbWeek.Enabled = false;
            this.cmbWeek.FormattingEnabled = true;
            this.cmbWeek.Location = new System.Drawing.Point(554, 60);
            this.cmbWeek.Name = "cmbWeek";
            this.cmbWeek.Size = new System.Drawing.Size(121, 21);
            this.cmbWeek.TabIndex = 16;
            // 
            // btnArr
            // 
            this.btnArr.Enabled = false;
            this.btnArr.Location = new System.Drawing.Point(207, 424);
            this.btnArr.Name = "btnArr";
            this.btnArr.Size = new System.Drawing.Size(271, 33);
            this.btnArr.TabIndex = 17;
            this.btnArr.Text = "Показать статистические параметры за период";
            this.btnArr.UseVisualStyleBackColor = true;
            this.btnArr.Click += new System.EventHandler(this.btnArr_Click);
            // 
            // cBEvening
            // 
            this.cBEvening.AutoSize = true;
            this.cBEvening.Enabled = false;
            this.cBEvening.Location = new System.Drawing.Point(398, 94);
            this.cBEvening.Name = "cBEvening";
            this.cBEvening.Size = new System.Drawing.Size(15, 14);
            this.cBEvening.TabIndex = 18;
            this.cBEvening.UseVisualStyleBackColor = true;
            // 
            // cBDay
            // 
            this.cBDay.AutoSize = true;
            this.cBDay.Enabled = false;
            this.cBDay.Location = new System.Drawing.Point(398, 74);
            this.cBDay.Name = "cBDay";
            this.cBDay.Size = new System.Drawing.Size(15, 14);
            this.cBDay.TabIndex = 19;
            this.cBDay.UseVisualStyleBackColor = true;
            // 
            // cBMoning
            // 
            this.cBMoning.AutoSize = true;
            this.cBMoning.Enabled = false;
            this.cBMoning.Location = new System.Drawing.Point(398, 54);
            this.cBMoning.Name = "cBMoning";
            this.cBMoning.Size = new System.Drawing.Size(15, 14);
            this.cBMoning.TabIndex = 20;
            this.cBMoning.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 469);
            this.Controls.Add(this.cBMoning);
            this.Controls.Add(this.cBDay);
            this.Controls.Add(this.cBEvening);
            this.Controls.Add(this.btnArr);
            this.Controls.Add(this.cmbWeek);
            this.Controls.Add(this.dGV);
            this.Controls.Add(this.btnFilter);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboEDate);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBDate);
            this.Controls.Add(this.lblPatronymic);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dGV)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblPatronymic;
        private System.Windows.Forms.ComboBox comboBDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboEDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn AverBP;
        private System.Windows.Forms.DataGridViewTextBoxColumn PBPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn LBPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn UBPColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn HRColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateColumn;
        private System.Windows.Forms.DataGridView dGV;
        private System.Windows.Forms.ComboBox cmbWeek;
        private System.Windows.Forms.Button btnArr;
        private System.Windows.Forms.CheckBox cBEvening;
        private System.Windows.Forms.CheckBox cBDay;
        private System.Windows.Forms.CheckBox cBMoning;
    }
}

