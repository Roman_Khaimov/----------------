﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PresentationAndProcessing.BL;
using PresentationAndProcessing.Data;

namespace PresentationAndProcessing
{
    public partial class Form1 : Form
    {

        private DataProcessor _DP;
        private Dictionary<string, DayOfWeek> _WeekCh;
        private Dictionary<string, TimeSpan> _TimeCh;
        private Dictionary<DateTime, PacientInformation> _ForArr;



        public Form1()
        {
            InitializeComponent();
            _WeekCh = new Dictionary<string, DayOfWeek>();
            _ForArr = new Dictionary<DateTime, PacientInformation>();
            _WeekCh.Add("Рабочие дни", DayOfWeek.Monday);
            _WeekCh.Add("Выходные дни", DayOfWeek.Sunday);
            _WeekCh.Add("Все дни", DayOfWeek.Saturday);
            _TimeCh = new Dictionary<string, TimeSpan>();
            _TimeCh.Add("Утро", new TimeSpan(6, 0, 0));
            _TimeCh.Add("День", new TimeSpan(12, 0, 0));
            _TimeCh.Add("Вечер", new TimeSpan(18, 0, 0));
            _TimeCh.Add("Целый день", new TimeSpan(23, 0, 0));
            cBMoning.Text = "Утро";
            cBDay.Text = "День";
            cBEvening.Text = "Вечер";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog opnd = new OpenFileDialog();
            opnd.Filter = "Текстовой файл.txt|*txt";

            if (opnd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                _DP = new DataProcessor(opnd.FileName);
                lblName.Text = _DP.GetPatient().Name;
                lblSurname.Text = _DP.GetPatient().Surname;
                lblPatronymic.Text = _DP.GetPatient().Patronymic;
                comboBDate.Enabled = true;
                comboEDate.Enabled = true;
                cBDay.Enabled = true;
                cBEvening.Enabled = true;
                cBMoning.Enabled = true;
                cmbWeek.Enabled = true;
                comboBDate.DataSource = _DP.GetDateTime();
                comboEDate.DataSource = _DP.GetDateTime();
                cmbWeek.DataSource = new List<string>(_WeekCh.Keys);
                btnFilter.Enabled = true;
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            dGV.Rows.Clear();
            _ForArr.Clear();
            if ((cBDay.Checked || cBEvening.Checked || cBMoning.Checked))
            {
                if (cBMoning.Checked)
                {
                    foreach (var data in _DP.GetInfo(Convert.ToDateTime(comboBDate.Text), Convert.ToDateTime(comboEDate.Text), _WeekCh[cmbWeek.Text], _TimeCh[cBMoning.Text]))
                    {
                        dGV.Rows.Add(data.Key.ToString(@"dd.MM.yyyy \/ HH:mm"), data.Value.HR, data.Value.UBP, data.Value.LBP, data.Value.PHP, data.Value.MHP);
                        _ForArr.Add(data.Key, data.Value);
                    }
                }
                if (cBDay.Checked)
                {
                    foreach (var data in _DP.GetInfo(Convert.ToDateTime(comboBDate.Text), Convert.ToDateTime(comboEDate.Text), _WeekCh[cmbWeek.Text], _TimeCh[cBDay.Text]))
                    {
                        dGV.Rows.Add(data.Key.ToString(@"dd.MM.yyyy \/ HH:mm"), data.Value.HR, data.Value.UBP, data.Value.LBP, data.Value.PHP, data.Value.MHP);
                        _ForArr.Add(data.Key, data.Value);
                    }
                }
                if (cBEvening.Checked)
                {
                    foreach (var data in _DP.GetInfo(Convert.ToDateTime(comboBDate.Text), Convert.ToDateTime(comboEDate.Text), _WeekCh[cmbWeek.Text], _TimeCh[cBEvening.Text]))
                    {
                        dGV.Rows.Add(data.Key.ToString(@"dd.MM.yyyy \/ HH:mm"), data.Value.HR, data.Value.UBP, data.Value.LBP, data.Value.PHP, data.Value.MHP);
                        _ForArr.Add(data.Key, data.Value);
                    }
                }
            }
            else
            {
                foreach (var data in _DP.GetInfo(Convert.ToDateTime(comboBDate.Text), Convert.ToDateTime(comboEDate.Text), _WeekCh[cmbWeek.Text], _TimeCh["Целый день"]))
                {
                    dGV.Rows.Add(data.Key.ToString(@"dd.MM.yyyy \/ HH:mm"), data.Value.HR, data.Value.UBP, data.Value.LBP, data.Value.PHP, data.Value.MHP);
                    _ForArr.Add(data.Key, data.Value);
                }
            }
            if (dGV.Rows.Count > 2)
                btnArr.Enabled = true;
            else
                btnArr.Enabled = false;
        }

        private void btnArr_Click(object sender, EventArgs e)
        {

            #region Init

            Form StForm = new Form();
            DataGridView sdGV = new DataGridView();
            sdGV.Columns.Add("DataClmn", "Параметр");
            sdGV.Columns.Add("HRClmn", "ЧСС");
            sdGV.Columns.Add("UBPClmn", "САД");
            sdGV.Columns.Add("LBPClmn", "ДАД");
            sdGV.Columns.Add("PHPClmn", "ПАД");
            sdGV.Columns.Add("AverBPClmn", "СрАД");
            StForm.Text = "Статистика за период";
            StForm.Size = new System.Drawing.Size(643, 264);
            sdGV.Size = new System.Drawing.Size(643, 264);
            sdGV.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right | AnchorStyles.Bottom;

            #endregion

            Label lbl = new Label();

            string str = "Статистика за период: " + Convert.ToDateTime(comboBDate.Text).ToString(@"dd\/MM\/yyyy") + "-" + Convert.ToDateTime(comboEDate.Text).ToString(@"dd\/MM\/yyyy");

            lbl.Text = str;

            lbl.TextAlign = ContentAlignment.MiddleCenter;

            lbl.Location = new Point(42, 24);

            lbl.Size = new System.Drawing.Size(600, 21);

            StForm.Controls.Add(lbl);
            StForm.Controls.Add(sdGV);

            sdGV.ReadOnly = true;
            sdGV.AllowUserToResizeColumns = false;
            sdGV.AllowUserToResizeRows = false;

            sdGV.Rows.Add("");

            #region Logic

 
            foreach (var data in _DP.GetAverValues(_ForArr))
            {
               sdGV.Rows.Add(data.Key, data.Value.HR, data.Value.UBP, data.Value.LBP, data.Value.PHP, data.Value.MHP);
            }

            #endregion


            StForm.Show();
        }


    }
}
