﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using PresentationAndProcessing.Data;

namespace PresentationAndProcessing.Data.Test
{
    [TestFixture]
    class PresentationAndProcessing
    {
        private Repository _repos;

        private const string filepath = @"C:\Users\про\Documents\Visual Studio 2013\Projects\PresentationAndProcessing\Example.txt";

        [SetUp]
        public void Init()
        {
            _repos = new Repository(filepath);
        }

        [Test]
        public void GetPatientNameRequred()
        {
            string name = _repos.GetPatientName();
            string EXPECTED_RESULT = "Василий";

            Assert.That(name, Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public void GetPatientSurnameRequred()
        {
            string surname = _repos.GetPatientSurname();
            string EXPECTED_RESULT = "Заренко";

            Assert.That(surname, Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public void GetPatientPatronymicRequred()
        {
            string patronymic = _repos.GetPatientPatronymic();
            string EXPECTED_RESULT = "Иванович";

            Assert.That(patronymic, Is.EqualTo(EXPECTED_RESULT));
        }

        [Test]
        public void GetDataRequred()
        {
            Dictionary<DateTime, PacientInformation> test = _repos.GetPatientInfo();



            Assert.That(test, Is.Not.Empty);
        }
    }
}
