﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentationAndProcessing.Data;

namespace PresentationAndProcessing.BL
{
    public class DataProcessor : IDataProcessor
    {

        private Repository _rep;
        private Patient _p;


        public DataProcessor(string filepath)
        {
            _rep = new Repository(filepath);
            _p = FormPatient();
        }

        private Patient FormPatient()
        {
            Patient patient = new Patient();

            patient.Name = _rep.GetPatientName();
            patient.Surname = _rep.GetPatientSurname();
            patient.Patronymic = _rep.GetPatientPatronymic();
            patient.Data = _rep.GetPatientInfo();
            Cilling(patient.Data);
            SetMHP(patient.Data);
            SetPHP(patient.Data);
            return patient;
        }

        public Patient GetPatient()
        {
            return _p;
        }

        public void Cilling(Dictionary<DateTime, PacientInformation> data)
        {
            List<double> averListHR = new List<double>();
            List<double> averListLBP = new List<double>();
            List<double> averListUBP = new List<double>();

            foreach (var inf in data.Values)
            {
                if ((inf.HR <= 40 || inf.HR >= 150))
                {
                    if (!(averListHR.Count == 0))
                        inf.HR = Math.Round(averListHR.Average(), 1);
                    else
                        inf.HR = 70; //Set default average value of HR = 70

                }
                averListHR.Add(inf.HR);

                if ((inf.LBP <= 40 || inf.LBP >= 140))
                {
                    if (!(averListLBP.Count == 0))
                        inf.LBP = Math.Round(averListLBP.Average(), 1);
                    else
                        inf.LBP = 85; //Set default average value of LBP = 85
                }
                averListLBP.Add(inf.LBP);
                if ((inf.UBP <= 60 || inf.UBP >= 260))
                {
                    if (!(averListUBP.Count == 0))
                        inf.UBP = Math.Round(averListUBP.Average(), 1);
                    else
                        inf.UBP = 130; //Set default average value of UBP = 130
                }
                averListUBP.Add(inf.UBP);
                //if ((inf.LBP > inf.UBP)) //If LBP is bigger than UBP. For example: Normal when - 130x85 and Not normal when - 120x140
                //{

                //}
            }
        }

        public void SetPHP(Dictionary<DateTime, PacientInformation> data)
        {
            foreach (var inf in data.Values)
            {
                inf.PHP = inf.UBP - inf.LBP;
            }
        }

        public void SetMHP(Dictionary<DateTime, PacientInformation> data)
        {
            foreach (var inf in data.Values)
            {
                inf.MHP = Math.Round((inf.UBP - inf.LBP) / 3 + inf.LBP, 1); //PHP is equal UBP - LBP
            }
        }

        public Dictionary<DateTime, PacientInformation> GetInfo(DateTime begin, DateTime end, DayOfWeek day, TimeSpan time)
        {
            Dictionary<DateTime, PacientInformation> data = new Dictionary<DateTime, PacientInformation>();
            Dictionary<DateTime, PacientInformation> result = new Dictionary<DateTime, PacientInformation>();

            foreach (var q in _p.Data)
            {
                if (begin <= q.Key && end >= q.Key)
                    data.Add(q.Key, q.Value);
            }

            data = DaysFilter(day, data);

            if (time == new TimeSpan(6, 0, 0)) //Morning
            {
                result = TimeFilter(new TimeSpan(6, 0, 0), new TimeSpan(12, 0, 0), data);
            }
            else if (time == new TimeSpan(12, 0, 0)) //Day
            {
                result = TimeFilter(new TimeSpan(12, 0, 0), new TimeSpan(18, 0, 0), data);
            }
            else if (time == new TimeSpan(18, 0, 0)) //Evening
            {
                result = TimeFilter(new TimeSpan(18, 0, 0), new TimeSpan(24, 0, 0), data);
            }
            else
            {
                result = data;
            }

            return result;
        }

        private Dictionary<DateTime, PacientInformation> TimeFilter(TimeSpan begin, TimeSpan end, Dictionary<DateTime, PacientInformation> data)
        {
                Dictionary<DateTime, PacientInformation> result = new Dictionary<DateTime, PacientInformation>();

                foreach (var q in data)
                {
                    if ((begin <= q.Key.TimeOfDay && q.Key.TimeOfDay <= end))
                    {
                        result.Add(q.Key, q.Value);
                    }
                }
                return result;
        }

        private Dictionary<DateTime, PacientInformation> DaysFilter(DayOfWeek day, Dictionary<DateTime, PacientInformation> data)
        {
            Dictionary<DateTime, PacientInformation> result = new Dictionary<DateTime,PacientInformation>();

            if (day == DayOfWeek.Monday)
            {
                foreach (var q in data)
                {
                    if (!(q.Key.DayOfWeek == DayOfWeek.Sunday || q.Key.DayOfWeek == DayOfWeek.Saturday))
                    {
                        result.Add(q.Key, q.Value);
                    }
                }
            }
            else if (day == DayOfWeek.Sunday)
            {
                foreach (var q in data)
                {
                    if (q.Key.DayOfWeek == DayOfWeek.Sunday || q.Key.DayOfWeek == DayOfWeek.Saturday)
                    {
                        result.Add(q.Key, q.Value);
                    }
                }
            }
            else
            {
                return data;
            }
            return result;
        }

        public Dictionary<string, PacientInformation> GetAverValues(Dictionary<DateTime, PacientInformation> data)
        {
            Dictionary<string, PacientInformation> result = new Dictionary<string, PacientInformation>();
            List<double> lstHR = new List<double>();
            List<double> lstUBP = new List<double>();
            List<double> lstLBP = new List<double>();
            List<double> lstPBP = new List<double>();
            List<double> lstAverBP = new List<double>();


            foreach (var inf in data.Values)
            {
                lstHR.Add(inf.HR);
                lstUBP.Add(inf.UBP);
                lstLBP.Add(inf.LBP);
                lstPBP.Add(inf.PHP);
                lstAverBP.Add(inf.MHP);
            }

            PacientInformation psnt = new PacientInformation();
            psnt.HR = Math.Round(lstHR.Average(), 1);
            psnt.UBP = Math.Round(lstUBP.Average(), 1);
            psnt.LBP = Math.Round(lstLBP.Average(), 1);
            psnt.PHP = Math.Round(lstPBP.Average(), 1);
            psnt.MHP = Math.Round(lstAverBP.Average(), 1);

            result.Add("M", psnt);

            PacientInformation psnte = new PacientInformation();

            psnte.HR = lstHR.Min();
            psnte.UBP = lstUBP.Min();
            psnte.LBP = lstLBP.Min();
            psnte.PHP = lstPBP.Min();
            psnte.MHP = lstAverBP.Min();

            result.Add("Min", psnte);

            PacientInformation psntet = new PacientInformation();

            psntet.HR = lstHR.Max();
            psntet.UBP = lstUBP.Max();
            psntet.LBP = lstLBP.Max();
            psntet.PHP = lstPBP.Max();
            psntet.MHP = lstAverBP.Max();

            result.Add("Max", psntet);

            PacientInformation psntete = new PacientInformation();

            psntete.HR = SD(lstHR);
            psntete.UBP = SD(lstUBP);
            psntete.LBP = SD(lstLBP);
            psntete.PHP = SD(lstPBP);
            psntete.MHP = SD(lstAverBP);

            result.Add("SD", psntete);


            return result;

        }


        private double SD(List<double> lst)
        {
            if (lst.Count != 1)
            {
                List<double> res = new List<double>();

                double Aver = Math.Round(lst.Average(), 1);

                foreach (var q in lst)
                {
                    res.Add(Math.Pow((q - Aver), 2));
                }

                double summ = res.Sum();

                summ = summ / (res.Count - 1);

                return Math.Round(Math.Sqrt(summ), 1);
            }
            return 0.0;
        }

        public List<DateTime> GetDateTime()
        {
            List<DateTime> s = new List<DateTime>();
            foreach (var time in this.GetPatient().Data.Keys)
            {
                s.Add(time);
            }
            return s;
        }

    }
}
