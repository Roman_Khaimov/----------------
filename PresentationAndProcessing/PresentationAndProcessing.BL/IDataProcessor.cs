﻿using PresentationAndProcessing.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationAndProcessing.BL
{
    public interface IDataProcessor
    {

        Patient GetPatient();
        void SetPHP(Dictionary<DateTime, PacientInformation> data);
        void SetMHP(Dictionary<DateTime, PacientInformation> data);
        Dictionary<DateTime, PacientInformation> GetInfo(DateTime begin, DateTime end, DayOfWeek day, TimeSpan time);
        Dictionary<string, PacientInformation> GetAverValues(Dictionary<DateTime, PacientInformation> data);
        void Cilling(Dictionary<DateTime, PacientInformation> data);

    }

}
