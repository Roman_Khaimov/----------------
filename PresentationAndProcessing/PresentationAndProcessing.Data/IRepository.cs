﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PresentationAndProcessing.Data;

namespace PresentationAndProcessing.Data
{
    public interface IRepository
    {
        Dictionary<DateTime, PacientInformation> GetPatientInfo();
        string GetPatientName();
        string GetPatientSurname();
        string GetPatientPatronymic();
    }
}
