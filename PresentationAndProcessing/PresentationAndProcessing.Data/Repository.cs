﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PresentationAndProcessing.Data
{
    public class Repository:IRepository
    {

        

        private StreamReader _StrRead;
        private string[] _Words;



        public Repository(string FilePath)
        {
            string s = "";
            _StrRead = new StreamReader(FilePath, System.Text.Encoding.GetEncoding(1251));
            while (!_StrRead.EndOfStream)
            {
                s += _StrRead.ReadLine() + Environment.NewLine;
            }
            string[] delimStr = { " - ", "ЧСС=", "САД=", "ДАД=", ", ", "\n", "\r", "Имя", "Фамилия", "Отчество" };
            _Words = s.Split(delimStr, StringSplitOptions.RemoveEmptyEntries);
        }

        public Dictionary<DateTime, PacientInformation> GetPatientInfo()
        {
            Dictionary<DateTime, PacientInformation> a = new Dictionary<DateTime, PacientInformation>();

            for (int i = 3; i < _Words.Length; i += 4)
            {
                PacientInformation ex = new PacientInformation();
                ex.HR = Convert.ToDouble(_Words[i + 1]);
                ex.UBP = Convert.ToDouble(_Words[i + 2]);
                ex.LBP = Convert.ToDouble(_Words[i + 3]);
                a.Add(Convert.ToDateTime(_Words[i]), ex);
            }
            return a;
        }

        public string GetPatientName()
        {
            return _Words[0];
        }

        public string GetPatientSurname()
        {
            return _Words[1];
        }

        public string GetPatientPatronymic()
        {
            return _Words[2];
        }
    }
}
