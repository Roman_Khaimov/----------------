﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PresentationAndProcessing.Data
{
    public class Patient
    {
       

        #region PrivateFields

        string _name;
        string _patronymic;
        string _surname;
        Dictionary<DateTime, PacientInformation> _data;

        #endregion


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        
        public string Surname
        {
            get { return _surname; }
            set { _surname = value; }
        }       

        public string Patronymic
        {
            get { return _patronymic; }
            set { _patronymic = value; }
        }

        public Dictionary<DateTime, PacientInformation> Data
        {
            get { return _data; }
            set { _data = value; }
        }

    }


    public class PacientInformation
    {
        public double UBP {get; set;}

        public double LBP { get; set; }

        public double HR  { get; set; }

        public double PHP { get; set; }

        public double MHP { get; set; }
    }
}


